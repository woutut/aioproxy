#!/usr/bin/env python
# -*- coding: utf-8 -*-s
from aiohttp import web, ClientSession, ClientProxyConnectionError, ClientConnectorError
from aiosocksy import SocksError
from aiosocksy.connector import ProxyConnector, ProxyClientRequest
import argparse


# Настраиваю пути
routes = web.RouteTableDef()


@routes.route('*', '/')
async def handle(request):

    print('- Request: {} ({})'.format(request.url, request.method))
    # Проверяю, разрешен ли метод
    if methods and request.method not in methods:
        # Если не разрешен - вовращаю 403 ошибку
        print('- Request forbidden')
        raise web.HTTPForbidden()
    print('- Request allowed')

    # Подготавливаю запрос
    conn = ProxyConnector(remote_resolve=False)
    try:
        async with ClientSession(
            connector=conn, request_class=ProxyClientRequest
        ) as session:
            # Подтягиваю source
            html = await fetch(session, request.url, request.method.lower())
            # Возвращаю данные
            return web.Response(text=html)
    # Обрабатываю ошибки
    except ClientProxyConnectionError as e:
        print('- Connection problem: {} ({})'.format(e, request))
    except ClientConnectorError as e:
        print('- SSL/certificate problem, etc: {} ({})'.format(e, request))
    except SocksError as e:
        print('- Socks communication problem: {} ({})'.format(e, request))
    except Exception as e:
        print('- Uknown problem: {} ({})'.format(e, request))


# Выполняю запрос
async def fetch(session, url, method):
    # Подтягиваю метод типа запроса из сессии
    async with getattr(session, method)(
        url, proxy='socks5://127.0.0.1:9050'
    ) as response:
        return await response.text()


# Если файл выполняется напрямую:
if __name__ == "__main__":

    # Подготавливаю аргументы
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, help='Server port')
    parser.add_argument('--methods', type=str, help='Allowed method')
    args = parser.parse_args()

    # Проверяю методы
    methods = args.methods.split(',') if args.methods else None
    print('- Allowed methods: {}'.format(methods))

    # Подготавливаю приложение
    app = web.Application()
    app.add_routes(routes)
    # Подтягиваю порт и стартую (8080 порт по умолчанию)
    web.run_app(app, port=args.port or 8080)
