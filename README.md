# AIOProxy
Tor proxy with async aiohttp webserver.

## Usage

Build image
```
docker build -t aioproxy_image .
```
Start detached container (you can specify proxy port and allowed methods)
```
docker run -d -ti --net=host --name aioproxy aioproxy_image --port=8080 --methods=GET,POST
```
Use as proxy for curl
```
curl -x localhost:8080 -X GET icanhazip.com
```

## Warnings

Doesn't work on Windows (no `--net=host` support).