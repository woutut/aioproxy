# Подготавливаю изображение
FROM ubuntu:18.04
LABEL author="Oleksandr Liebiediev"
LABEL description="AIOProxy"

# Проставляю кодировку Python
ENV PYTHONIOENCODING=utf-8
# Игнорирую Dialog Ошибки
ENV DEBIAN_FRONTEND noninteractive

# Обновляю репозиториии устанавливаю Tor
RUN apt-get update && apt-get install -y tor \
    # Корректирую и применяю конфигурацию
    && sed -i 's/^#SOCKSPort 9050/SOCKSPort 0.0.0.0:9050/' /etc/tor/torrc

# Устанавливаю CURL и Python3
RUN apt-get install -y curl python3 python3-pip

# Копирую список зависимостей
WORKDIR /home/intel471
COPY requirements.txt requirements.txt

# Обновляю версию pip и устанавливаю целевые библиотеки
# Необходимо устанавливать по отдельности из-за импорта
RUN pip3 install pip --upgrade
RUN pip3 install -r requirements.txt

# Копирую приложение
COPY aioproxy.py aioproxy.py
# Копирую скрипт запуска
COPY start.sh start.sh

# Стартую Tor и приложение
ENTRYPOINT ["bash", "start.sh"]